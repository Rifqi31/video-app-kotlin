package com.example.videoapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// main activity class
class MainActivity : AppCompatActivity() {
    // init with data type
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: DataAdapter
    private lateinit var dataArrayList: ArrayList<DataModel>
    // onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        // context view layout xml
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // supplies call data
        addData()
        // var myString: String = resources.getString(R.string.app_name)
        // bind data into recycler view using adapter
        recyclerView = findViewById<View>(R.id.recycler_view) as RecyclerView
        adapter = DataAdapter(dataArrayList)
        // provide layout manager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@MainActivity)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

        // call item touch listener
        recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(this,
                recyclerView, object : ClickListener {
                    // on click method
                    override fun onClick(view: View?, position: Int) {
                        //Values are passing to activity & to fragment as well
//                        Toast.makeText(
//                            this@MainActivity, "Single Click on position        :$view",
//                            Toast.LENGTH_SHORT
//                        ).show()
                        // send value from list into another activity / screen
                        val intent = Intent(this@MainActivity, DetailActivity::class.java).apply {
                            putExtra("video_title", dataArrayList[position].titleVideo)
                            putExtra("video_description", dataArrayList[position].infoVideo)
                            putExtra("video_file", dataArrayList[position].videoUrl)
                        }
                        startActivity(intent)
                    }
                    // long click method
                    override fun onLongClick(view: View?, position: Int) {
//                        Toast.makeText(
//                            this@MainActivity, "Long press on position :$position",
//                            Toast.LENGTH_LONG
//                        ).show()
                    }
                })
        )

    }

    // add data into recycler view
    fun addData() {
        dataArrayList = ArrayList<DataModel>()
        dataArrayList.add(DataModel(
            "Sample format video mp4",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "http://techslides.com/demos/sample-videos/small.mp4"))
        dataArrayList.add(DataModel(
            "Sample format video m3u8",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"))
        dataArrayList.add(DataModel(
            "Sample format video webm",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "http://techslides.com/demos/sample-videos/small.webm"))
        dataArrayList.add(DataModel(
            "Sample high res mp4",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "http://mirrors.standaloneinstaller.com/video-sample/jellyfish-25-mbps-hd-hevc.mp4"))
        dataArrayList.add(DataModel(
            "Sample med res mp4",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "http://mirrors.standaloneinstaller.com/video-sample/dolbycanyon.mp4"))
        dataArrayList.add(DataModel(
            "Another high res mp4",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "http://mirrors.standaloneinstaller.com/video-sample/star_trails.mp4"))
    }


    // touch interface
    interface ClickListener {
        fun onClick(view: View?, position: Int)
        fun onLongClick(view: View?, position: Int)
    }

    // touch listener
    internal class RecyclerTouchListener(
        context: Context?,
        recycleView: RecyclerView,
        private val clicklistener: ClickListener?
    ) :
        RecyclerView.OnItemTouchListener {
        private val gestureDetector: GestureDetector
        override fun onInterceptTouchEvent(
            rv: RecyclerView,
            e: MotionEvent
        ): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

        init {
            gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child =
                        recycleView.findChildViewUnder(e.x, e.y)
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child))
                    }
                }
            })
        }
    }
}

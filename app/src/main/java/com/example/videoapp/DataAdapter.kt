package com.example.videoapp

import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*


class DataAdapter(dataList: ArrayList<DataModel>?) : RecyclerView.Adapter<DataAdapter.DataViewHolder>() {
    private val dataList: ArrayList<DataModel>?
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.list_item, parent, false)
        return DataViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    override fun onBindViewHolder(holder: DataAdapter.DataViewHolder, position: Int) {
        holder.containTitle.text = dataList!![position].titleVideo
        holder.containInfo.text = dataList!![position].infoVideo
    }

    inner class DataViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val containTitle: TextView
        val containInfo: TextView

        init {
            containTitle = itemView.findViewById(R.id.video_name_title)
            containInfo = itemView.findViewById(R.id.detail_info)
        }
    }

    init {
        this.dataList = dataList
    }
}

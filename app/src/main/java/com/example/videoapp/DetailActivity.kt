package com.example.videoapp

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.configuration.PlayerConfig
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem

// detail activity class
class DetailActivity : AppCompatActivity() {

    // property
    lateinit var mPlayerView: JWPlayerView
    // on create function
    override fun onCreate(savedInstanceState: Bundle?) {
        // context view layout xml
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // receive data intent from list clicked
        val catchVideoTitle: String? = intent.getStringExtra("video_title")
        val catchVideoDescription: String? = intent.getStringExtra("video_description")
        val catchVideoFile: String? = intent.getStringExtra("video_file")

        // apply video title into layout by id
        findViewById<TextView>(R.id.detail_title).apply {
            text = catchVideoTitle
        }

        // apply description into layout Id
        findViewById<TextView>(R.id.detail_description).apply {
            text = catchVideoDescription
        }

        // create playlist video
        mPlayerView = findViewById(R.id.jwplayer)
        val playlistItem = PlaylistItem.Builder()
            .file(catchVideoFile) // url file
            .title(catchVideoTitle) // title video
            .description("A woman who disappointed her self to kill a dragon ") // description
            .image("https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Sintel_poster.jpg/640px-Sintel_poster.jpg") // image cover
            .build()
        val playlist: MutableList<PlaylistItem> = ArrayList()
        playlist.add(playlistItem)
        val config = PlayerConfig.Builder()
            .playlist(playlist)
            .build()
        mPlayerView.setup(config)

    }

    // jw player function
    // play
    override fun onStart() {
        super.onStart()
        mPlayerView.onStart()
    }

    // resume
    override fun onResume() {
        super.onResume()
        mPlayerView.onResume()
    }

    // pause
    override fun onPause() {
        super.onPause()
        mPlayerView.onPause()
    }

    // top
    override fun onStop() {
        super.onStop()
        mPlayerView.onStop()
    }

    // destroy
    override fun onDestroy() {
        super.onDestroy()
        mPlayerView.onDestroy()
    }
}
